#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define len 500

int* alloue_tableau(int n){
    int *T=(int*)malloc(sizeof(int)*n);
    return T;
}
void desalloue_tableau(int *T) {
    free(T);
}
void remplir_tableau(int *tab,int n,int V){
    for(int i=0;i<n;i++){
        // valeurs aléatoire entre [0,V[
        tab[i]=rand()%V;
    }
}
void afficher_tableau(int *tab,int n){
    for(int i=0;i<n;i++){
        printf("%d ",tab[i]);
    }
    printf("\n");
}
int somme_carre(int *tab,int n){
    int somme=0;
    int j,i;
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            somme+=(tab[i]-tab[j])*(tab[i]-tab[j]);
        }
    }
    return somme;
}

int somme_carre2(int *tab,int n){
    int tmp1=0;
    int tmp2=0;
    for(int i=0;i<n;i++){
        tmp1+=tab[i];
        tmp2+=tab[i]*tab[i];
    }
    tmp1=tmp1*tmp1;
    return 2*(n*tmp2-tmp1);
}

int main(){
    srand(time(NULL));
    int *tab = alloue_tableau(len);
    remplir_tableau(tab,len,11);
    afficher_tableau(tab,len);
    
    clock_t tps_init;
    clock_t tps_final;
    double tps_cpu;
    
    tps_init=clock();
    somme_carre2(tab,len);
    //printf("%d\n",somme_carre(tab,len));
    tps_final=clock();
    tps_cpu=((double)(tps_final-tps_init))/CLOCKS_PER_SEC;
    printf("%d %f \n",len,tps_cpu);
    //printf("%d\n",somme_carre2(tab,len));
    desalloue_tableau(tab);
    return 0;
}
