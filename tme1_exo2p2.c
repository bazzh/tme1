#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define len 5

int** alloue_matrice(int n){
    //On alloue n lignes
    int **mat=(int**)malloc(sizeof(int*)*n);
    if(mat==NULL){
        fprintf(stderr,"Erreur d'allocation mémoire");exit(1);
    }
    int j;
    //On alloue les cases de chaque ligne
    for(int i=0;i<n;i++){
        mat[i]=(int*)malloc(sizeof(int)*n);
        if(mat[i]==NULL){
            for(j=0;j<i;j++){ //on libère l'espace déjà alloué avant de libérer la matrice
                free(mat[j]);
            }
            free(mat);
            return NULL;
        }
    }
    return mat;
}
void desalloue_matrice(int *mat[len]){
    // On libère les cases avant de libérer les lignes
    for(int i=0;i<len;i++)
        free((int*)mat[i]);
    free(mat);
}
void remplir_matrice(int *mat[len],int V){
    int i,j;
    for(i=0;i<len;i++){
        for(j=0;j<len;j++){
            // valeurs aléatoire entre [0,V[
            mat[i][j]=rand()%V;
        }
    }
}
void afficher_matrice(int *mat[len]){
    int i,j;
    for(i=0;i<len;i++){
        for(j=0;j<len;j++)
            printf("%d ",mat[i][j]);
        printf("\n"); //Saut de ligne par passer à la prochaine ligne
    }
}

int val_diff1(int *mat[len]){
    int i,j,k,l;
    for(i=0;i<len;i++){
        for(j=0;j<len;j++){
            for(k=i+1;k<len;k++){
                for(l=j+1;l<len;l++){
                    //On compare chaque case au reste, on parcourt 2x la matrice
                    if(mat[i][j]==mat[k][l] && (i!=k && j!=l))
                        return 0;
                }
            }
        }
    }
    return 1;
}
int val_diff2(int *mat[len],int V){
    int *tab=(int*)malloc(sizeof(int)*V);
    if(tab==NULL){
        fprintf(stderr,"Erreur d'allocation mémoire");exit(1);
    }
    // on initiallise seulement la 1ère case pour ne pas avoir tab[0]==0
    tab[0]=-1;
    if((len*len)>V){
        free(tab);
        return 0;
    }else{
        int i,j;
        for(i=0;i<len;i++){
            for(j=0;j<len;j++){
                /* si la case du tab vaut 0 (ou -1 pour la 1ère valeur) cela signifie 
                qu'on rencontre la valeur pour la 1ère fois */
                if((tab[mat[i][j]]==0 && mat[i][j]!=0) || tab[mat[i][j]]==-1){
                    //if permettant de contrôler si il y a deux 0
                    if(mat[i][j]==0 && tab[0]==0){
                        free(tab);
                        return 0;
                    }
                    tab[mat[i][j]]=mat[i][j];
                }else{
                    free(tab);
                    return 0;
                }
            }
        }
    }
    free(tab);
    return 1;
}

int** produit(int *mat1[len],int*mat2[len]){
    int **res=alloue_matrice(len);
    if(res==NULL){
        fprintf(stderr,"Erreur d'allocation mémoire");exit(1);
    }
    int i,j,k;
    for(i=0;i<len;i++){
        for(j=0;j<len;j++){
            res[i][j]=0; //On initialise la matrice "résultat" à 0
            for(k=0;k<len;k++){
                // On effectue la somme de ligne i (matrice1) * colonne j (matrice2) 
                res[i][j]+=mat1[i][k]*mat2[k][j];
            }
        }
    }
    return res;
}

int** produit2(int *mat1[len],int *mat2[len]){
    return NULL;
}

int main(){
    srand(time(NULL));
    int v=1000000000;
    
    int **mat=alloue_matrice(len);
    if(mat==NULL){
        fprintf(stderr,"Erreur d'allocation mémoire");exit(1);
    }
    
    /* test de la fonction produit
    int **mat2=alloue_matrice(len); 
    remplir_matrice(mat,v);
    afficher_matrice(mat);
    printf("\n");
    remplir_matrice(mat2,v);
    afficher_matrice(mat2);
    printf("\n");
    
    afficher_matrice(produit(mat,mat2));
    */
    
    // test des fonctions de la questions 2.5
    remplir_matrice(mat,v);
    //afficher_matrice(mat);
    clock_t tps_init;
    clock_t tps_final;
    double tps_cpu;
    tps_init=clock();
    if(val_diff2(mat,v))
        printf("les valeurs sont différentes v2\n");
    
    //if(val_diff1(mat))
       // printf("les valeurs sont différentes\n");
    
    tps_final=clock();
    tps_cpu=((double)(tps_final-tps_init))/CLOCKS_PER_SEC;
    printf("%d %f \n",len,tps_cpu);
    
    desalloue_matrice(mat);
    return 0;
}

